<?php

namespace Metrogistics\AzureSocialite;

class AzureException extends \Exception
{
    protected $description;

    public function __construct($error, $description, $previous = null) {
        $this->description = $description;

        parent::__construct($error, 0, $previous);
    }

    public function getDescription()
    {
        return $this->description;
    }
}
